var TSC = TSC || {};

TSC.embedded_config_xml = '<x:xmpmeta xmlns:x="adobe:ns:meta/">\
   <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:xmp="http://ns.adobe.com/xap/1.0/" xmlns:xmpDM="http://ns.adobe.com/xmp/1.0/DynamicMedia/" xmlns:xmpG="http://ns.adobe.com/xap/1.0/g/" xmlns:tsc="http://www.techsmith.com/xmp/tsc/" xmlns:xmpMM="http://ns.adobe.com/xap/1.0/mm/" xmlns:tscDM="http://www.techsmith.com/xmp/tscDM/" xmlns:tscIQ="http://www.techsmith.com/xmp/tscIQ/" xmlns:tscHS="http://www.techsmith.com/xmp/tscHS/" xmlns:stDim="http://ns.adobe.com/xap/1.0/sType/Dimensions#" xmlns:stFnt="http://ns.adobe.com/xap/1.0/sType/Font#" xmlns:exif="http://ns.adobe.com/exif/1.0" xmlns:dc="http://purl.org/dc/elements/1.1/">\
      <rdf:Description tsc:version="2.0.1" dc:date="2015-03-25 08:56:08 AM" dc:source="Camtasia Studio,8.5.0,enu" dc:title="REVEAL_Introduction" tscDM:firstFrame="REVEAL_Introduction_First_Frame.png" tscDM:originId="C590E3E5-E46B-4BD2-81F4-1DEAB9D79A9D" tscDM:project="REVEAL_Introduction">\
         <xmpDM:duration xmpDM:scale="1/1000" xmpDM:value="212266"/>\
         <xmpDM:videoFrameSize stDim:unit="pixel" stDim:h="720" stDim:w="1280"/>\
         <tsc:langName>\
            <rdf:Bag>\
               <rdf:li xml:lang="en-US">English</rdf:li></rdf:Bag>\
         </tsc:langName>\
         <xmpDM:Tracks>\
            <rdf:Bag>\
               <rdf:li>\
                  <rdf:Description xmpDM:trackType="Hotspot" xmpDM:frameRate="f1000" xmpDM:trackName="Hotspots">\
                     <xmpDM:markers>\
                        <rdf:Seq>\
                           <rdf:li><rdf:Description xmp:label="1" xmpDM:startTime="23270" xmpDM:duration="178800" tscDM:boundingPoly="765,149;953,149;953,179;765,179;" tscDM:rotate="0.000000" tscHS:pause="1" tscHS:jumpTime="114833"/></rdf:li><rdf:li><rdf:Description xmp:label="2" xmpDM:startTime="23270" xmpDM:duration="178800" tscDM:boundingPoly="765,119;953,119;953,149;765,149;" tscDM:rotate="0.000000" tscHS:pause="1" tscHS:jumpTime="94667"/></rdf:li><rdf:li><rdf:Description xmp:label="3" xmpDM:startTime="23270" xmpDM:duration="178800" tscDM:boundingPoly="765,90;953,90;953,119;765,119;" tscDM:rotate="0.000000" tscHS:pause="1" tscHS:jumpTime="56800"/></rdf:li><rdf:li><rdf:Description xmp:label="4" xmpDM:startTime="23270" xmpDM:duration="178800" tscDM:boundingPoly="765,60;953,60;953,90;765,90;" tscDM:rotate="0.000000" tscHS:pause="1" tscHS:jumpTime="32333"/></rdf:li></rdf:Seq>\
                     </xmpDM:markers>\
                  </rdf:Description>\
               </rdf:li>\
            </rdf:Bag>\
         </xmpDM:Tracks>\
         <tscDM:controller>\
            <rdf:Description xmpDM:name="tscplayer">\
               <tscDM:parameters>\
                  <rdf:Bag>\
                     <rdf:li xmpDM:name="autohide" xmpDM:value="true"/><rdf:li xmpDM:name="autoplay" xmpDM:value="false"/><rdf:li xmpDM:name="loop" xmpDM:value="false"/><rdf:li xmpDM:name="searchable" xmpDM:value="false"/><rdf:li xmpDM:name="captionsenabled" xmpDM:value="false"/><rdf:li xmpDM:name="sidebarenabled" xmpDM:value="false"/><rdf:li xmpDM:name="unicodeenabled" xmpDM:value="false"/><rdf:li xmpDM:name="backgroundcolor" xmpDM:value="FFFFFF"/><rdf:li xmpDM:name="sidebarlocation" xmpDM:value="left"/><rdf:li xmpDM:name="endaction" xmpDM:value="stop"/><rdf:li xmpDM:name="endactionparam" xmpDM:value="true"/><rdf:li xmpDM:name="locale" xmpDM:value="en-US"/></rdf:Bag>\
               </tscDM:parameters>\
               <tscDM:controllerText>\
                  <rdf:Bag>\
                  </rdf:Bag>\
               </tscDM:controllerText>\
            </rdf:Description>\
         </tscDM:controller>\
         <tscDM:contentList>\
            <rdf:Description>\
               <tscDM:files>\
                  <rdf:Seq>\
                     <rdf:li xmpDM:name="0" xmpDM:value="REVEAL_Introduction.mp4"/><rdf:li xmpDM:name="1" xmpDM:value="REVEAL_Introduction_First_Frame.png"/></rdf:Seq>\
               </tscDM:files>\
            </rdf:Description>\
         </tscDM:contentList>\
      </rdf:Description>\
   </rdf:RDF>\
</x:xmpmeta>';
