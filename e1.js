var fs = require('fs');
var fs1 = require('fs');
var dataMine;
var file = 'eventsData.json';
var robocopy = require('robocopy');

fs1.readFile(file, 'utf8', function(err, data2) {
	if (err) {
		console.log('Error: ' + err);
		return;
	}
	dataMine = JSON.parse(data2);
});

var jade = require('jade');

fs.readFile('template.jade', 'utf8', function(err, data) {
	if (err) throw err;

	var fn = jade.compile(data);
	var beautify = require('js-beautify').html;
	var html = fn({
		name: dataMine.pageSpecs[0].mainParagraphContent
	});

	fs.writeFile(dataMine.pageSpecs[0].fileName + '.html', beautify(html, {
		indent_size: 2
	}), function(err) {
		if (err) {
			return console.log(err);
		}
		console.log("The file was saved!");
	});

});

robocopy({

	// Specifies the path to the source directory.
	source: __dirname,

	// Specifies the destination path(s).
	destination: '//mt-schweb02-l/Websites/Marketing/InfoTrack_2015',

	// Indicates if multiple destinations should be copied serialy. By default 
	// multiple destinations are copied in parallel.


	// Specifies the file or files to be copied. You can use wildcard characters (* or ?), if
	// you want. If the File parameter is not specified, *.* is used as the default value.
	files: ['*.html', '*.js']
})

.fail(function(error) {
	console.log(error.message);
});